## About Project

This project is example CRUD App in Laravel. It is use Laravel version 8.x.x

## Some Project Screenshot

1. [Create-Foods.png](https://postimg.cc/BtKpgJdM)
2. [Detail-Foods.png](https://postimg.cc/m1YQGg44)
3. [List-Foods.png](https://postimg.cc/qzQ3cYM1)
4. [Update-Foods.png](https://postimg.cc/xJKkXp4y)
