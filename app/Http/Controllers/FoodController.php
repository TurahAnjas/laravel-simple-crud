<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Models\M_Food;

class FoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = M_Food::latest()->paginate(5);
        return view('pages.food.index')->with([
            'data' => $data
        ])->with(request()->input('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.food.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'food_name' => 'required',
            'food_category' => 'required',
            'food_image' => 'required'
        ]);
        $data = new M_Food();
        $data->food_name = $request->input('food_name');
        $data->food_category = $request->input('food_category');
        if ($request->hasFile('food_image')) {
            $file = $request->file('food_image');
            $extension = $file->clientExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/food/', $filename);
            $data->food_image = $filename;
        }
        $data->save();
        return redirect('/')->with('success', 'Successfully Save New Food');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = M_Food::findOrFail($id);
        return view('pages.food.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_Food::findOrFail($id);
        return view('pages.food.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'food_name' => 'required',
            'food_category' => 'required'
        ]);
        $item = M_Food::findOrFail($id);
        $item->food_name = $request->input('food_name');
        $item->food_category = $request->input('food_category');
        if ($request->hasFile('food_image')) {
            $destination_path = 'uploads/food/' . $item->food_image;
            if (File::exists($destination_path)) {
                File::delete($destination_path);
            }
            $file = $request->file('food_image');
            $extension = $file->clientExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/food/', $filename);
            $item->food_image = $filename;
        }
        $item->update();
        return redirect('/')->with('success', 'Successfully Update Food');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = M_Food::findOrFail($id);
        $destination_path = 'uploads/food/' . $item->food_image;
        if (File::exists($destination_path)) {
            File::delete($destination_path);
        }
        $item->delete();
        return redirect('/')->with('success', 'Successfully Delete Food');
    }
}
