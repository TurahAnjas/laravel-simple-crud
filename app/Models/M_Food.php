<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class M_Food extends Model
{
    use SoftDeletes;
    protected $table = 'food';
    protected $fillable = ['food_name', 'food_category', 'food_image'];
}
