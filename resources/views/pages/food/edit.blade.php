@extends('layouts.default')
@section('content')
    <section>
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-12 my-3">
                    <div class="float-start">
                        <h1>Edit Food</h1>
                    </div>
                    <div class="float-end">
                        <a href="{{ url('/') }}" class="btn btn-secondary">
                            Back</a>
                    </div>
                </div>
                <div class="col-lg-8 mx-auto">
                    <div class="row">
                        <div class="col-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problem with yout input <br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="col-12">
                            <form action="{{ url('/update/' . $data->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mt-2">
                                    <label for="food_name">Food Name *</label>
                                    <input class="form-control" type="text" name="food_name" id="food_name"
                                        value="{{ $data->food_name }}" placeholder="Fried Rice" required>
                                </div>
                                <div class="form-group mt-2">
                                    <label for="food_category">Food Category *</label>
                                    <input class="form-control" type="text" name="food_category" id="food_category"
                                        value="{{ $data->food_category }}" placeholder="Rice" required>
                                </div>
                                <div class="form-group mt-2">
                                    <label class="w-100"><strong class="mr-2">Current Food Image: </strong></label>
                                    <img class="img-fluid w-50" src="{{ asset('uploads/food/' . $data->food_image) }}" alt="{{ $data->food_name }}">
                                </div>
                                <div class="form-group mt-2">
                                    <label for="food_image"><strong>Food Image</strong> *</label>
                                    <input class="form-control" type="file" name="food_image" id="food_image" placeholder="Select Image">
                                </div>
                                <div class="form-group mt-4">
                                    <button class="btn btn-success" type="submit">Save Data</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
