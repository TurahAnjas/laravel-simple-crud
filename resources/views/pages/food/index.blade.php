@extends('layouts.default')
@section('content')
    <section>
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-10 mx-auto">
                    <div class="float-start">
                        <h1>Simple CRUD Laravel</h1>
                    </div>
                    <div class="float-end">
                        <a href="{{ url('create') }}" class="btn btn-primary">+ Add Food</a>
                    </div>
                </div>
                <div class="col-lg-10 mt-2 mx-auto">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success" role="alert">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                </div>
                <div class="col-lg-10 mt-2 mx-auto">
                    <table class="table table-bordered">
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-center">Name</th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Image</th>
                            <th class="text-center">Action</th>
                        </tr>
                        @if (count($data) === 0)
                            <tr>
                                <td class="text-center text-muted small fst-italic fw-bolder" colspan="5">Food Data Not
                                    Found</td>
                            </tr>
                        @endif
                        @foreach ($data as $food)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{{ $food->food_name }}</td>
                                <td>{{ $food->food_category }}</td>
                                <td class="text-center">
                                    <img class="img-fluid" src="{{ asset('uploads/food/' . $food->food_image) }}"
                                        alt="{{ $food->food_name }}">
                                </td>
                                <td class="text-center">
                                    <a href="{{ url('/show/' . $food->id) }}" class="btn btn-info m-2">Show</a>
                                    <a href="{{ url('/edit/' . $food->id) }}" class="btn btn-warning m-2">Edit</a>
                                    <a href="{{ url('/delete/' . $food->id) }}" class="btn btn-danger m-2">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <p>
                        Displaying {{ $data->count() }} of {{ $data->total() }} food(s).
                    </p>
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </section>
@endsection
