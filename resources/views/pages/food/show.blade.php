@extends('layouts.default')
@section('content')
    <section>
        <div class="container">
            <div class="row my-5">
                <div class="col-lg-12 my-3">
                    <div class="float-start">
                        <h1>Detail Food</h1>
                    </div>
                    <div class="float-end">
                        <a href="{{ url('/') }}" class="btn btn-secondary">
                            Back</a>
                    </div>
                </div>
                <div class="col-lg-8 mx-auto">
                    <div class="form-group mt-2">
                        <strong class="mr-2">Food Name: </strong>
                        {{ $data->food_name }}
                    </div>
                    <div class="form-group mt-2">
                        <strong class="mr-2">Food Category: </strong>
                        {{ $data->food_category }}
                    </div>
                    <div class="form-group mt-2">
                        <strong class="mr-2">Food Image: </strong>
                        <img class="img-fluid" src="{{ asset('uploads/food/' . $data->food_image) }}" alt="{{ $data->food_name }}">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
