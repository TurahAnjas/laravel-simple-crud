<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FoodController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FoodController::class, 'index']);
Route::get('/create', [FoodController::class, 'create']);
Route::get('/edit/{id}', [FoodController::class, 'edit']);
Route::get('/show/{id}', [FoodController::class, 'show']);
Route::get('/delete/{id}', [FoodController::class, 'destroy']);
Route::post('/store', [FoodController::class, 'store']);
Route::post('/update/{id}', [FoodController::class, 'update']);
